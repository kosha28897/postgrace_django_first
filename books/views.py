from django.http import HttpResponse,Http404,HttpResponseRedirect
from django.shortcuts import render
from books.models import Book
import datetime
from books.forms import ContactForm
from django.core.mail import send_mail,get_connection

# Create your views here.
def test(request):
    #var = request.path
    #var=request.META['HTTP_USER_AGENT']
    #var = request.META['HTTP_REFERER']
    var=request.META.get('HTTP_USER_AGENT','unknown')
    return HttpResponse(var)

def display_meta(request):
    values=request.META
    html=[]
    for k in sorted(values):
        html.append("<tr><td> %s </td> <td> %s </td></tr>" %(k,values[k]))
    return HttpResponse('<table>%s</table>' %'\n'.join(html))

def search_form(request):
    return render(request,'books/search_form.html')

def search(request):
    error=[]
    if 'q' in request.GET:

       # message='you searched for: %r' %request.GET['q']
        q=request.GET['q']
        if not q:
            error.append('enter a search term')
        elif len(q)>=20:
            error.append('keyword should be of at most 20 char.')
        else:
            books=Book.objects.filter(title__icontains=q)
            return render(request,'books/search_result.html',{'books':books,'query':q})

       # message='you submited an empty form.'
    return render(request,'books/search_form.html',{'error':error})


def contact(request):
    if request.method=='POST':
        form=ContactForm(request.POST)
        if form.is_valid():

            cd=form.cleaned_data
            con=get_connection('django.core.mail.backends.smtp.EmailBackend')
            send_mail(cd['subject'],cd['message'],cd.get('email', 'testmails28897@gmail.com'),['v.kosha3110@gmail.com'],fail_silently=False,connection=con)
            return HttpResponseRedirect('/contact/thanks/')
    else:
        form=ContactForm(initial={'subject':'feedback'})
    return render(request,'contact_form.html',{'form':form})


def thanks(request):
    return HttpResponse('thanks')






